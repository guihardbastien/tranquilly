package scala.tranquilly.services

import net.liftweb.json.{DefaultFormats, parse}
import spark.Spark.get
import spark.{Request, Response}

import scala.collection.mutable
import scala.tranquilly.services.AlertProcess.Alert

/**
 * retrieves history from DB
 */
object HistoryAndTrackingService {
  case class TrackingResponse(alert: Alert, user_id: String, user_name: String)
  implicit val formats: DefaultFormats.type = DefaultFormats

  val hist: String = "{\n" +
    "  \"name\": \"Romain\",\n" +
    "  \"parentName\": \"Maman de Romain\",\n" +
    "  \"id\": \"b94d27b9934d3e01\",\n" +
    "  \"alertHistory\": [\n" +
    "    {\n" +
    "      \"coordinates\": {\n" +
    "        \"lon\": 10,\n" +
    "        \"lat\": 10\n      },\n" +
    "      \"timestamp\": 100,\n" +
    "      \"reason\": \"TRACKING\"\n" +
    "    },\n" +
    "    {\n" +
    "      \"coordinates\": {\n" +
    "        \"lon\": 10,\n" +
    "        \"lat\": 10\n" +
    "      },\n" +
    "      \"timestamp\": 101,\n" +
    "      \"reason\": \"TRACKING\"\n" +
    "    }\n" +
    "  ],\n" +
    "  \"messageHistory\": [\n" +
    "    {\n" +
    "      \"coordinates\": {\n" +
    "        \"lon\": 10,\n" +
    "        \"lat\": 10\n" +
    "      },\n" +
    "      \"timestamp\": 100,\n" +
    "      \"message\": \"coucou maman\"\n" +
    "    },\n" +
    "        {\n" +
    "      \"coordinates\": {\n" +
    "        \"lon\": 10,\n" +
    "        \"lat\": 10\n" +
    "      },\n" +
    "      \"timestamp\": 101,\n" +
    "      \"message\": \"j'ai mangé le chat\"\n" +
    "    }\n" +
    "  ]\n" +
    "}"

  /**
   * Mocking documents from DB
   */
  val historyDbDocuments: mutable.Map[String, String] = mutable.HashMap(
    "b94d27b9934d3e01" -> hist,
  )

  val serverPort: Int = 18080
  val AuthorizationHeader: String = "Authorization"

  // kids
  val validTokens: mutable.Map[String, String] = mutable.HashMap(
    "b94d27b9934d3e10" -> "Maman de Romain",
    "b94d27b9934d3e11" -> "Maman de NicolasF",
    "b94d27b9934d3e12" -> "Maman de NicolasL",
  )

  def main(args: Array[String]): Unit = {
    get(
      "/api/v1/track",
      (request: Request, response: Response) => {

        val user_id_child = request.queryParams("user_id")
        if (user_id_child.isEmpty) {
          response.status(400)
          "(/api/v1/track) : User_id can't be empty"
        }
        val bearer = request.headers(AuthorizationHeader)
        val userName = checkBearerAndGetName(bearer);
        if (userName.isEmpty) {
          // 401: Unauthorized
          response.status(401)
          "(HistoryAndTrackingService) : Unauthorized user"
        } else {
          // 200: OK
          response.status(200)
          processTrackInformation(userName.get, user_id_child).toString
        }
      }
    )
    get(
      "/api/v1/history/alert",
      (request: Request, response: Response) => {
        val user_id_child = request.queryParams("user_id")
        if (user_id_child.isEmpty) {
          response.status(400)
          "(/api/v1/track) : User_id can't be empty"
        }
        val bearer = request.headers(AuthorizationHeader)
        val userName = checkBearerAndGetName(bearer);
        if (userName.isEmpty) {
          // 401: Unauthorized
          response.status(401)
          "(HistoryAndTrackingService) : Unauthorized user"
        } else {
          processHistoryInformation(userName.get, user_id_child, "alert")
          // 200: OK
          response.status(200)
          ""
        }
      }
    )
    get(
      "/api/v1/history/message",
      (request: Request, response: Response) => {
        val user_id_child = request.queryParams("user_id")
        if (user_id_child.isEmpty) {
          response.status(400)
          "(/api/v1/track) : User_id can't be empty"
        }
        val bearer = request.headers(AuthorizationHeader)
        val userName = checkBearerAndGetName(bearer);
        if (userName.isEmpty) {
          // 401: Unauthorized
          response.status(401)
          "(HistoryAndTrackingService) : Unauthorized user"
        } else {
          processHistoryInformation(userName.get, user_id_child, "message")
          // 200: OK
          response.status(200)
          ""
        }
      }
    )
  }

  /**
   * Check information in dataBase to return Track Information
   *
   * @param user_name_parent
   * @param user_id_kid
   */
  def processTrackInformation(user_name_parent: String, user_id_kid: String): TrackingResponse = {
    val json = parse(historyDbDocuments(user_id_kid))

    val histArr = json \ "alertHistory"
    val arr = histArr.extract[List[Alert]]
    val firstAlert = arr.head

    val kidName = json \ "name"
    val user_name = kidName.extract[String]

    return TrackingResponse(firstAlert, user_id_kid, user_name)
  }

  /**
   * Check history en fonction de la valeur history (message, alert)
   *
   * @param user_name_parent
   * @param user_id_kid
   * @param process
   */
  def processHistoryInformation(user_name_parent: String, user_id_kid: String, process: String): Unit = {
    //Check 404 Error
    //Todo Check in database history for user_id_kid
    println("--------------- PROCESS HISTORY INFORMATION ---------------------------")
    /*
    * user_id
    * user_name
    * timestamp
    * server_timestamp
    * coordinates
    * reason || message
    * */
    val hist = historyDbDocuments.get(user_id_kid)


  }

  /**
   * returns null if unauthorized else returns username
   *
   * @param bearer
   * @return
   */
  def checkBearerAndGetName(bearer: String): Option[String] = {
    if (bearer == null) {
      Option.empty
    } else {
      val fields = bearer.split("\\s+", 2)
      if (fields.size == 2 && fields(0) == "Bearer" && validTokens.contains(fields(1))) {
        println("-------------- SENDREQUEST POST ------------------ ")
        // FIXME
        //sendRequest.post("http://localhost:18080/api/v1/user?query", "{\"bearer_token\":\""+ fields(1)+"\"}")
        validTokens.get(fields(1))
      } else {
        Option.empty
      }
    }
  }
}
