package scala.tranquilly.services

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.Serdes

import scala.jdk.CollectionConverters._

/**
 * Set of producers
 */
object MessageAndAlertProducer {
  val producer =
    new KafkaProducer[String, String](
      Map[String, AnyRef](
        ProducerConfig.BOOTSTRAP_SERVERS_CONFIG -> "localhost:9092"
      ).asJava,
      Serdes.String().serializer(),
      Serdes.String().serializer()
    )

  /**
   * Shove data down the right topic
   *
   * @param token
   * @param content
   * @param topicName
   */
  def alertAndMessageProducer(token: String, content: String, topicName: String): Unit = {
    val record = new ProducerRecord[String, String](topicName, token, token + "\n" + content)
    producer.send(record).get()
  }

}
