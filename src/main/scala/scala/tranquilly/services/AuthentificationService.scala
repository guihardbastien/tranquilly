package scala.tranquilly.services

import spark.Spark.post
import spark.{Request, Response}

import scala.collection.mutable
import scala.tranquilly.services.http.SendRequest


/**
 * Check the token received by API (Message & Alert)
 */
object AuthentificationService {
  val serverPort: Int = 18080
  val sendRequest: SendRequest = new SendRequest
  val AuthorizationHeader: String = "Authorization"

  // kids
  val validTokens: mutable.Map[String, String] = mutable.HashMap(
    "b94d27b9934d3e01" -> "Romain",
    "b94d27b9934d3e02" -> "NicolasF",
    "b94d27b9934d3e08" -> "NicolasL",
  )

  def main(args: Array[String]): Unit = {
    post(
      "/api/v1/alert",
      (request: Request, response: Response) => {
        val bearer = request.headers(AuthorizationHeader)
        val userName = checkBearerAndGetName(bearer);
        println("---------- CHECKKKK BEARERRRR SUCCESSFULLYY ------------" + userName)
        if (userName.isEmpty) {
          // 401: Unauthorized
          response.status(401)
          "(AuthentificationService) : Unauthorized user"
        } else {
          processMessageAndAlert(request, response, bearer, "alertTopic")
          // 200: OK
          response.status(200)
          ""
        }
      }
    )
    post(
      "/api/v1/message",
      (request: Request, response: Response) => {
        val bearer = request.headers(AuthorizationHeader)
        val userName = checkBearerAndGetName(bearer);
        if (userName.isEmpty) {
          // 401: Unauthorized
          response.status(401)
          "(AuthentificationService) : Unauthorized user"
        } else {
          processMessageAndAlert(request, response, bearer, "messageTopic")
          // 200: OK
          response.status(200)
          ""
        }
      }
    )
  }

  /**
   * processMessageAndAlert
   *
   * @param request
   * @param response
   * @param bearer
   */
  def processMessageAndAlert(request: Request, response: Response, bearer: String, topic: String): Unit = {
    //Check 404 Error
    println("PROCESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS")
    if (bearer.isEmpty) {
      response.status(404).toString
    } else {
      val content = request.body()
      try {
        //Todo verify body
        MessageAndAlertProducer.alertAndMessageProducer(bearer, content, topic) // 200: Ok
        response.status(200)
        "200: OK"
      } catch {
        case e: Exception =>
          // 500: Internal server error
          throw e
      }
    }
  }

  /**
   * returns null if unauthorized else returns username
   *
   * @param bearer
   * @return
   */
  def checkBearerAndGetName(bearer: String): String = {
    if (bearer == null) {
      ""
    } else {
      val fields = bearer.split("\\s+", 2)
      if (fields.size == 2 && fields(0) == "Bearer" && validTokens.contains(fields(1))) {
        println("-------------- SENDREQUEST POST ------------------ ")
        // FIXME
        sendRequest.get("http://localhost:18080/api/v1/user",fields(1))
        //validTokens.get(fields(1))
      } else {
        ""
      }
    }
  }

}
