package scala.tranquilly.services

import spark.Spark.{port, post}
import spark.{Request, Response}

import scala.collection.mutable
import scala.tranquilly.services.AuthentificationService.sendRequest
import scala.tranquilly.services.removemeauthserviceoldlegacy.{AuthorizationHeader, isAuthorized}


/**
 * Request DataBase to get the user_id corresponding to the token
 */
object UserServiceAPI {

  // Set of tokens considered as valid
  // TODO replace with DB call
  val validTokens: mutable.Map[String, String] = mutable.HashMap(
    "b94d27b9934d3e01" -> "Zizou",
    "b94d27b9934d3e02" -> "Bernie Tapar",
    "b94d27b9934d3e08" -> "Trump",
  )

  def main(args: Array[String]): Unit = {
    port(18080)
    post(
      "http://localhost:18080/api/v1/user",
      (request: Request, response: Response) => {
        println("USSSSSSSSSSSSSSSSSSSSSSSSSSEEEEEEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRR")
        val user_id_child = request.queryParams("user_id")
        if (user_id_child.isEmpty) {
          response.status(400)
          "(/api/v1/track) : User_id can't be empty"
        }
        println("DADZDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" + user_id_child)
        val verificationToken = getToken(user_id_child)
        if(verificationToken.isEmpty){
          response.status(401)
          "UserServiceAPI : Unauthorized user"
        } else {
          response.status(200)
          "" + validTokens.get(verificationToken)
        }
      }
    )
  }

  /**
   * returns null if unauthorized else returns username
   *
   * @param bearer
   * @return
   */
  def getToken(request: String): String = {
    if (request == null) {
      ""
    } else {
      val fields = request.split(" : ", 2)
      println("piiiiiiiiiiiiiiiiiii" + fields.mkString("Array(", ", ", ")"))
      if (fields.size == 2 && fields(0) == "\"bearer_token\"") {
        fields(1)
      }
      ""
    }
  }
}
