package scala.tranquilly.services.process

import net.liftweb.json.Serialization.write
import net.liftweb.json.{DefaultFormats, parse}
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.serialization.Serdes

import java.time.Duration
import scala.collection.mutable
import scala.tranquilly.services.http.SendRequest

object AlertProcess {

  case class Coord(x: Long, y: Long)

  case class Alert(coordinates: Coord, time: Long, reason: String)

  case class Request(to_id: String,
                     to_name: String,
                     from_id: String,
                     from_name: String,
                     reason: String,
                     serverTimeStamp: Long,
                     time_stamp: Long)

  //---------------------------------------------
  // Data while we can't get information from dataBase
  val validTokens: mutable.Map[String, String] = mutable.HashMap(
    "b94d27b9934d3e01" -> "Romain",
    "b94d27b9934d3e02" -> "NicolasF",
    "b94d27b9934d3e08" -> "NicolasL",
  )

  val validTokensParents: mutable.Map[String, String] = mutable.HashMap(
    "1" -> "Maman de Romain",
    "2" -> "Papa de Romain",
    "3" -> "Frere de Romain",
    "4" -> "Maman de NicolasF",
    "5" -> "Papa de NicolasF",
    "6" -> "Maman de NicolasL",
    "7" -> "Papa de NicolasL",
  )

  val families: mutable.Map[String, List[String]] = mutable.HashMap(
    "b94d27b9934d3e01" -> List("1","2","3"),
    "b94d27b9934d3e02" -> List("4","5"),
    "b94d27b9934d3e08" -> List("6","7"),
  )
  //---------------------------------------------

  val sendRequest: SendRequest = new SendRequest

  import scala.jdk.CollectionConverters._

  implicit val formats = DefaultFormats

  val consumer =
    new KafkaConsumer[String, String](
      Map[String, AnyRef](
        ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "localhost:9092",
        ConsumerConfig.GROUP_ID_CONFIG -> s"${getClass.getCanonicalName}",
        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest"
      ).asJava,
      Serdes.String().deserializer(),
      Serdes.String().deserializer()
    )

  def main(args: Array[String]): Unit = {
    try {
      consumer.subscribe(List("alertTopic").asJava)
      while (true) {
        val records: ConsumerRecords[String, String] =
          consumer.poll(Duration.ofSeconds(5))
        if (!records.isEmpty) {
          records.asScala.foreach(record => {
            // json is a JValue instance
            if (record.toString.length < 10) {
              println(record)
            } else {
              //First shot of json parsing
              val str = record.value();
              val strs = str.split("\n", 2);
              val token = strs(0)
              println("-----------------------"+token)
              val user_name = validTokens.get(token)
              var content = extractJsonToRequest(strs(1))
              //Todo Add token & content to database
              if (content.reason == "REDBUTTON" || content.reason == "ZONEOUT") {
                sendToFamily(token, user_name.get, content)
              }
            }
          })
        } else {
          println("no nore message")
        }
        Thread.sleep(5000)
      }
    } finally {
      consumer.close()
    }
  }

  /**
   * Extract data from json String
   *
   * @param content
   * @return Alert
   */
  def extractJsonToRequest(content: String): Alert ={
      val json = parse(content)
      val coordinates = json \ "coordinates"
      val objectCoord = coordinates.extract[Coord]
      val time = json \ "time"
      val resTime = time.extract[Long]
      val reason = json \ "reason"
      val resReaon = reason.extract[String]
      val data = Alert(objectCoord, resTime, resReaon)
      data
  }

  /**
   * Send the request to AlertAPI to alert everyone attached to the child sending the alert
   *
   * @param token
   * @param content
   */
  def sendToFamily(token : String, user_name : String, content : Alert): Unit = {
    //Build the request
    if(validTokens.contains(token)){
      val listMember = families.get(token)
      for(member <- listMember.get){
        val user_name_Parent = validTokensParents.get(member)
        val request = Request(token, user_name, member, user_name_Parent.get, content.reason, content.time, content.time)
        val jsonString = write(request)
        println("Json result : "+jsonString)
        //sendRequest.post("localhost:18080/api/v1/alert/notify", jsonString)
      }
    }
  }

}
