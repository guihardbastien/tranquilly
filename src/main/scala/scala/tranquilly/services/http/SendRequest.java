package scala.tranquilly.services.http;

import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.Buffer;

public class SendRequest {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    public String post(String url, String json) throws IOException, JSONException {
        var mediaType = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(mediaType, String.valueOf(new JSONObject().getJSONObject(json))); // new
        System.out.println("CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+body);
        // RequestBody body = RequestBody.create(JSON, json); // old
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println("-------------- SENDREQUEST BODY ------------------ ");
        return response.body().string();
    }

    public String get(String url, String bearer_token) throws IOException {
            HttpUrl.Builder urlBuilder
                    = HttpUrl.parse(url).newBuilder();
            urlBuilder.addQueryParameter("bearer_token", bearer_token);

            String url_http = urlBuilder.build().toString();
        System.out.println("UUUURLLLLLLLLLLLLLLLLLLLLLLLLLL "+ url_http);
            Request request = new Request.Builder()
                    .url(url_http)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().toString();
    }
}
