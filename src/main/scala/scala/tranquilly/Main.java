package scala.tranquilly;

import scala.tranquilly.services.*;

public class Main {
    public static void main(String[] args) {
        var producer = new Thread(()->{
            AuthentificationService.main(null);
        });

        var consumer = new Thread(()->{
            UserServiceAPI.main(null);
        });

        var trackingAndHistoryService = new Thread(()->{
            HistoryAndTrackingService.main(null);
        });

        producer.start();
        consumer.start();
        trackingAndHistoryService.start();
    }
}
