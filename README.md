# tranquilly


## Installation

### Step 1: Preparing your Ubuntu Server

Update your fresh Ubuntu 20.04 server and get Java installed as illustrated below.
```
sudo apt update && sudo apt upgrade
sudo apt install default-jre wget git unzip -y
sudo apt install default-jdk -y
```
### Step 2: Fetch Kafka on Ubuntu 20.04

After Java is well installed, let us now fetch Kafka sources. Head over to Downloads and look for the Latest release and get the sources under Binary downloads. Click on the one that is recommended by Kafka and you will be redirected to a page that has a link you can use to fetch it.
```
cd ~
wget https://downloads.apache.org/kafka/3.0.0/kafka_2.13-3.0.0.tgz
sudo mkdir /usr/local/kafka-server && cd /usr/local/kafka-server
sudo tar -xvzf ~/kafka_2.13-3.0.0.tgz --strip 1 
cd /usr/local/kafka-server
```
### Step 3 : Launch servers
Ouvrir nouveau terminal :
```
sudo bin/zookeeper-server-start.sh config/zookeeper.properties 
```
Ouvrir nouveau terminal
```
sudo bin/kafka-server-start.sh config/server.properties
```
### Step 4 : Create topic
Ouvrir nouveau terminal
``` 
sudo bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test 
```

## Tests

### Alert and Message API
To test Alert and Message API, you need to post an HTTP request with these elements :
Authorization: Bearer token
Request :
```
localhost:18080/api/v1/message
```
Body (json) Alert:
```
{
    "ccordinates" :{
        "x" : 1,
        "y" : 2
    },
    "timestamp": 1,
    "reason" : "REDBUTTON"
}
```
Body (json) Message:
```
{
    "ccordinates" :{
        "x" : 1,
        "y" : 2
    },
    "timestamp": 1,
    "reason" : "REDBUTTON"
}
```
#### Result : 
Json result will appear in the log


### Track and History API
To test Track and History API, you need to make a get HTTP request like this :
```
localhost:18080/api/v1/track?user_id=b94d27b9934d3e01
```